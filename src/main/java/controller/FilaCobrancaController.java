package controller;

import enumProperties.Status;
import form.CobrancaForm;
import io.javalin.http.Context;
import model.Cobranca;
import model.FilaCobranca;

public class FilaCobrancaController {
	
	private FilaCobranca filaCobranca = new FilaCobranca();
	
	private FilaCobrancaController() {
		
	}
	
	public static void filaCobranca (Context ctx){
    	
		try {
			CobrancaForm cobrancaJson = ctx.bodyAsClass(CobrancaForm.class);
	    	Cobranca cobranca = cobrancaJson.unserialize();
			FilaCobrancaController daoFilaCobrancaController = new FilaCobrancaController();	    	    	
	    	FilaCobranca filaCobranca = daoFilaCobrancaController.retornarFilaCobrancaPreenchida();
	    	filaCobranca.setQueueCobranca(cobranca);
	    	ctx.status(200);
	    	ctx.json(filaCobranca);
		}catch(Exception ex) {
			ctx.status(422);
		}
		
    }
    

  	public void deletarCobranca() {
  		FilaCobrancaController daoFilaController = new FilaCobrancaController();
  		FilaCobranca queue = daoFilaController.retornarFilaCobrancaPreenchida();
  		
  		Cobranca cobrancaDeletada = queue.getQueueCobranca().remove();
  		CobrancaController cobrar = new CobrancaController();
  		
  		cobrar.setCobranca(cobrancaDeletada);
  		cobrar.sendCobranca();  		
  	}
  	
  	public FilaCobranca retornarFilaCobrancaPreenchida() {
  		FilaCobranca filaCobrancas = new FilaCobranca();
  		filaCobrancas.getQueueCobranca().add(new Cobranca("3fa85f62-5717-4562-b3fc-2c963f66afa8", "2019-09-03T13:23:41.414Z", "2019-09-03T13:25:29.195Z", 10, Status.PENDENTE)) ;  		
  		return filaCobrancas;  		
  	}
  
  	public FilaCobranca getFilaCobranca() {
		return filaCobranca;
	}
}