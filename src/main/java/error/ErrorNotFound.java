package error;

public class ErrorNotFound extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private String messageCartaoNotFound = "Cartão não encontrado.";
	
	private String messageCobrancaNotFound = "Cobrança não encontrada.";
	
	private String messageTransactionUnathorized = "Transação não autorizada.";

	public ErrorNotFound() {

	}

	public ErrorNotFound(String message) {
		
	}

	public ErrorNotFound(String messageCartaoNotFound, String messageCobrancaNotFound, String messageTransactionUnathorized) {
		this.messageCartaoNotFound = messageCartaoNotFound;
		this.messageCobrancaNotFound = messageCobrancaNotFound;
		this.messageTransactionUnathorized = messageTransactionUnathorized;
	}

	public String getMessageCartaoNotFound() {
		return messageCartaoNotFound;
	}

	public void setMessageCartaoNotFound(String messageCartaoNotFound) {
		this.messageCartaoNotFound = messageCartaoNotFound;
	}

	public String getMessageCobrancaNotFound() {
		return messageCobrancaNotFound;
	}

	public void setMessageCobrancaNotFound(String messageCobrancaNotFound) {
		this.messageCobrancaNotFound = messageCobrancaNotFound;
	}

	public String getMessageTransactionUnathorized() {
		return messageTransactionUnathorized;
	}

	public void setMessageTransactionUnathorized(String messageTransactionUnathorized) {
		this.messageTransactionUnathorized = messageTransactionUnathorized;
	}
}
