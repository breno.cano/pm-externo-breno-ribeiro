package controller;

import java.text.SimpleDateFormat;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import enumProperties.Status;
import error.ErrorNotFound;
import form.CobrancaForm;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import model.Cobranca;
import model.CreditCard;
import model.FilaCobranca;
import pagamento.TransactionCielo;

public class CobrancaController {

	private static final String MERCHANTID = "556d2973-d792-49e7-a6af-323c67eaf332";
	private static final String MERCHANTKEY = "KTDALYUUGYWMDRFQXKKFOSQLSSMIPANFXKBOUDLK";
	private static final String REQUISICAOCIELO = "https://apisandbox.cieloecommerce.cielo.com.br/1/sales/";

	private CreditCard card;
	private Cobranca cobranca;

	public CobrancaController (){
		
	}

	public CobrancaController (CreditCard card, Cobranca cobranca){
		this.card = card;
		this.cobranca = cobranca;
	}

	public static void getCobranca(Context ctx) throws ErrorNotFound {
		CobrancaController daoCobrancaController = new CobrancaController();
		Cobranca daoCobranca = daoCobrancaController.consultarCobrancaCobrancaById(Objects.requireNonNull(ctx.pathParam("id")));

		if (daoCobranca != null) {
			ctx.status(200);
			ctx.json(daoCobranca);
		} else {
			ErrorNotFound cobNotFound = new ErrorNotFound();
			ctx.html(cobNotFound.getMessageCobrancaNotFound());
			ctx.status(404);
		}
	}

	public static void realizaCobranca(Context ctx) {
		Cobranca cobrancaForm = ctx.bodyAsClass(CobrancaForm.class).deserializePostCobranca();
		CreditCardController cardController = new CreditCardController();
		CreditCard cardRetornado = cardController.retornaCartaoDeCreditoIntegracao();
		CobrancaController send = new CobrancaController(cardRetornado, cobrancaForm);

		HttpResponse<JsonNode> response = send.sendCobranca();
		ctx.json(response.getBody());

		if (response.getStatus() == 201) {
			ctx.status(201);
			return;
		} else {
			ctx.status(422);
		}
	}

	public static String obterDatetime() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return formatter.format(date);
	}

	public ArrayList<Cobranca> retornarCobrancas() {

		ArrayList<Cobranca> cobrancasRetornadas = new ArrayList<Cobranca>();
		cobrancasRetornadas.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", 1));
		cobrancasRetornadas
				.add(new Cobranca("5fc808e4-d59d-4556-8ee7-b2d754b8cdf1", "3fa85f64-5717-4562-b3fc-2c963f66afa7",
						"2021-09-03T13:23:41.414Z", "2021-09-03T13:25:29.195Z", 7, Status.PENDENTE));
		
		for (int i = 0; i < cobrancasRetornadas.size(); i++) {
			FilaCobranca.inserir(cobrancasRetornadas.get(i));
		}
		
		return cobrancasRetornadas;
	}

	public Cobranca consultarCobrancaCobrancaById(String id) throws ErrorNotFound {

		ArrayList<Cobranca> cobrancasIdLista = retornarCobrancas();
		Cobranca cobrancaId = null;

		for (int i = 0; i < cobrancasIdLista.size(); i++) {
			if (cobrancasIdLista.get(i).getId().equals(id)) {
				cobrancaId = cobrancasIdLista.get(i);
				break;
			}
		}

		if (cobrancaId != null) {
			return cobrancaId;
		} else {
			ErrorNotFound error = new ErrorNotFound();
			throw new ErrorNotFound(error.getMessageCobrancaNotFound());
		}
	}
	
	public HttpResponse<JsonNode> sendCobranca() {
		TransactionCielo transaction = new TransactionCielo("1");
		transaction.getCustomer().setName(this.card.getNomeTitular());
		transaction.getPayment().setAmount(cobranca.getValor());
		transaction.getPayment().setInstallments(1);
		transaction.getPayment().setType("CreditCard");
		transaction.getPayment().getCreditCard().setBrand("Visa");
		transaction.getPayment().getCreditCard().setCardNumber(card.getNumCartao());
		transaction.getPayment().getCreditCard().setExpirationDate(card.getDataValidade());
		transaction.getPayment().getCreditCard().setSecurityCode(card.getCvv());
		HttpResponse<JsonNode> response = Unirest.post(REQUISICAOCIELO).header
				("Content-Type", "application/json").header("MerchantKey", MERCHANTKEY)
				.header("MerchantId", MERCHANTID).body(transaction).asJson();
		return response;
	}
	
	public HttpResponse<JsonNode> validaCreditCard(CreditCard card){
		TransactionCielo transaction = new TransactionCielo("1");
		transaction.getPayment().setAmount(1);
		transaction.getPayment().setInstallments(1);
		transaction.getPayment().setType("CreditCard");
		transaction.getPayment().getCreditCard().setBrand("Visa");
		transaction.getPayment().getCreditCard().setCardNumber(card.getNumCartao());
		transaction.getPayment().getCreditCard().setExpirationDate(card.getDataValidade());
		transaction.getPayment().getCreditCard().setSecurityCode(card.getCvv());
		HttpResponse<JsonNode> response = Unirest.post(REQUISICAOCIELO).header
				("Content-Type", "application/json").header("MerchantKey", MERCHANTKEY)
				.header("MerchantId", MERCHANTID).body(transaction).asJson();
		return response;
	}
	
	public Cobranca getCobranca() {
		return cobranca;
	}
	

	public void setCobranca(Cobranca cobranca) {
		this.cobranca = cobranca;
	}
}
