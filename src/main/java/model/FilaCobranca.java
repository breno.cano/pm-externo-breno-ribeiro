package model;

import java.util.Queue;

public class FilaCobranca {
	
	private static Queue<Cobranca> queueCobranca;

	public static void inserir(Cobranca cobranca) {
		queueCobranca.add(cobranca);
	}
	
	public Queue<Cobranca> getQueueCobranca() {
		return queueCobranca;
	}

	public static void setQueueCobranca(Queue<Cobranca> queueCobranca) {
		FilaCobranca.queueCobranca = queueCobranca;
	}

	public void setQueueCobranca(Cobranca cobranca) {
		FilaCobranca.queueCobranca.add(cobranca);
	}
}
