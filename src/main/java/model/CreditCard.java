package model;

import java.util.UUID;

public class CreditCard {

	private String id;
	private String nomeTitular;
	private String numCartao;
	private String dataValidade;
	private String cvv;

	public CreditCard() {

	}

	public CreditCard(String nomeTitular, String numCartao, String dataValidade, String cvv) {
		this.id = UUID.randomUUID().toString();
		this.nomeTitular = nomeTitular;
		this.numCartao = numCartao;
		this.dataValidade = dataValidade;
		this.cvv = cvv;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public String getNumCartao() {
		return numCartao;
	}

	public void setNumCartao(String numCartao) {
		this.numCartao = numCartao;
	}

	public String getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(String dataValidade) {
		this.dataValidade = dataValidade;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getBrand() {
		if (this.getNumCartao().substring(0, 1).equals("4")) {
			return "Visa";
		} else if (this.getNumCartao().substring(0, 1).equals("5")) {
			return "MasterCard";
		} else if (this.getNumCartao().substring(0, 2).equals("37")) {
			return "AmericanExpress";
		} else {
			return "Sem bandeira";
		}
	}
}