package main.util;

import static io.javalin.apibuilder.ApiBuilder.get;

import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import controller.CobrancaController;
import controller.CreditCardController;
import controller.EmailController;
import controller.FilaCobrancaController;
import io.javalin.Javalin;

public class JavalinApp {
	private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {		
                   path("/cobranca/:id", ()-> get(CobrancaController::getCobranca));
                   path("/cobranca",  ()-> post(CobrancaController::realizaCobranca));
                   path("/filaCobranca",  () -> post(FilaCobrancaController::filaCobranca));
                   path("/validaCartaoDeCredito",  ()-> post(CreditCardController::validaCartaoDeCredito));
                   path("/enviarEmail",  ()-> post(EmailController::enviarEmail));
                    });
                   
    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
    
}
