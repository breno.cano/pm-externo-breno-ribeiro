package controller;

import java.util.ArrayList;

import org.json.JSONObject;

import error.ErrorNotFound;
import form.CreditCardForm;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import model.CreditCard;


public class CreditCardController {
	
	// Variável responsável por armazenar o endpoint da integração
	// Do tipo "final", pq não é permitido ser alterada
	private final String pathIntegracao = "https://scb-pm.herokuapp.com/cartaoDeCredito";
	
	public CreditCard retornaCartaoDeCreditoIntegracao() {
		CreditCard card = new CreditCard("Breno PM Externo", "4556346904266753", "03/2024", "118");
		return card;
	}

	public static boolean validaCartaoDeCredito(Context ctx) {
		CreditCard cardForm = ctx.bodyAsClass(CreditCardForm.class).deserialize();
		CobrancaController validation = new CobrancaController();
		HttpResponse<JsonNode> result = validation.validaCreditCard(cardForm);

		if (result.getStatus() == 201) {
			ctx.status(200);
			return true;
		} else if (result.getStatus() == 402) {
			ctx.status(402);
			return false;
		} else if (result.getStatus() == 422) {
			ctx.status(422);
			return false;
		}
		return false;
	}
	
	public static ArrayList<CreditCard> retornaListaCartaoDeCreditoIntegracao() {
		ArrayList<CreditCard> cardsLista = new ArrayList<CreditCard>();
		cardsLista.add(new CreditCard("Breno PM Externo", "4556346904266753", "03/2024", "118"));
		return cardsLista;								
	}
	
	public static CreditCard verificarCards(String titular) throws ErrorNotFound {
		ArrayList<CreditCard> cards = retornaListaCartaoDeCreditoIntegracao();
		CreditCard card = null;
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).getNomeTitular().equals(titular)) {
				card = cards.get(i);
				break;
			}
		}
		if (card != null) {
			return card;
		} else {
			ErrorNotFound cardNotFound = new ErrorNotFound();
			throw new ErrorNotFound(cardNotFound.getMessageCartaoNotFound());
		}
	}
	
	// Método responsável por realizar a integração
	public CreditCard getCartaoDeCreditoIntegracaoById(String id) {
		// Linha responsável pela integração do microsserviço Aluguel - Cartão de Crédito, do tipo Get
		HttpResponse<JsonNode> response = Unirest.get(pathIntegracao + id).header("accept", "application/json").asJson();		
		kong.unirest.json.JSONObject json = response.getBody().getObject();
		CreditCard card = new CreditCard(json.getString("nomeTitular"),  json.getString("numCartao"), json.getString("dataValidade"), json.getString("cvv"));
		return card;		
	}
}
