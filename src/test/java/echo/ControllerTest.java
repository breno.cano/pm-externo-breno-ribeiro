package echo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.mockito.Mockito.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import controller.CreditCardController;
import error.ErrorNotFound;

import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import kong.unirest.json.JSONObject;
import main.util.JavalinApp;
import model.Cobranca;
import model.CreditCard;

class ControllerTest {

	private static JavalinApp app = new JavalinApp();

	@BeforeAll
	static void init() {
		app.start(7010);
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}

	// Testes de Unidade

	private CreditCard card = new CreditCard();

	@Test
	void testGetBrand() {
		card.setNumCartao("4556346904266753");
		assertEquals("Visa", card.getBrand());
	}

	@Test
	void testExcecaoCartaoNaoEncontrado() throws ErrorNotFound {
		assertThrows(ErrorNotFound.class, () -> {
			CreditCardController.verificarCards("Breno Ribeiro PmExt");
		});
	}

	@Test
	void testConsultaCartao() throws ErrorNotFound {
		assertEquals("4556346904266753", CreditCardController.verificarCards("Breno PM Externo").getNumCartao());
	}

	@Test
	void testeRetornarCartoes() {
		assertEquals(1, CreditCardController.retornaListaCartaoDeCreditoIntegracao().size());
	}

	// Testes de Integração
	
	@Test
	void testPostCobranca() {
		HttpResponse<JsonNode> response = Unirest.post("https://pm-externo.herokuapp.com/cobranca")
				.header("accept", "application/json").queryString("ciclistaId", "5fc808e4-d59d-4556-8ee7-b2d754b8cdf1")
				.queryString("valor", "5").asJson();
		assertNotNull(response.getBody());
	}
	
	@Test
	void testRealizarCobranca() {
		HttpResponse<JsonNode> resposta = Unirest.post("https://pm-externo.herokuapp.com/cobranca")
				.header("accept", "application/json").queryString("ciclistaId", "5fc808e4-d59d-4556-8ee7-b2d754b8cdf1")
				.queryString("valor", "5").asJson();

		JSONObject json = resposta.getBody().getObject();
		Cobranca cobran = new Cobranca(json.optString("id"), json.optInt("valor"));

		assertEquals("0", "" + cobran.getValor());
	}
	
	@Test
	void testPostCobrancaStatus() {
		HttpResponse<JsonNode> response = Unirest.post("https://pm-externo.herokuapp.com/cobranca")
				.body("{\"ciclistaId\":\"5fc808e4-d59d-4556-8ee7-b2d754b8cdf1\", \"valor\":\"1\"}").asJson();

		assertEquals(201, response.getStatus());
	}
	
	
	// Testes com Erros
	
	/*
	@Test
	void testGetCobranca() {
		HttpResponse<String> response = Unirest
				.get("https://pm-externo.herokuapp.com/cobranca/5fc808e4-d59d-4556-8ee7-b2d754b8cdf1").asString();
		assertEquals(200, response.getStatus());
	}	

	@Test
	void testPostSendEmail() {
		HttpResponse<JsonNode> response = Unirest.post("https://pm-externo.herokuapp.com/enviarEmail")
				.body("{\"message\":\"Teste email\", \"email\":\"breno.cano@uniriotec.br\"}").asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void testPostQueueCobranca() {
		HttpResponse<JsonNode> response = Unirest.post("https://pm-externo.herokuapp.com/filaCobranca")
				.body("{\"ciclistaId\":\"3fa85f62-5717-4562-b3fc-2c963f66af72\",\"horaSolicitacao\":\"2020-10-03T13:22:21.414Z\", \"horaFinalizacao\":\"2020-10-03T13:25:27.195Z\", \"valor\":\"15\", \"status\":\"PAGA\"}")
				.asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void testPostValidateCreditCard() {
		HttpResponse<JsonNode> response = Unirest.post("https://pm-externo.herokuapp.com/validaCartaoDeCredito").body(
				"{\"nomeTitular\": \"Teste\", \"numCartao\":\"0001.0002.0003.0001\", \"dataValidade\":\"10/2027\", \"cvv\":\"321\"}")
				.asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
    void testGetCartaoDeCreditoIntegracaoById() {    	         	
       HttpResponse response = Unirest.get("https://https://scb-pm.herokuapp.com/cartaoDeCredito/5fc808e4-d59d-4556-8ee7-b2d754b8cdf1").asString();
       assertEquals(422, response.getStatus());     
    }
	*/
}
