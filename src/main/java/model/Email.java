package model;

import java.util.UUID;

public class Email {
	
	private String id;
	private String message;
	private String emailDest;
	
	public Email() {
		
	}

	public Email(String message, String emailDest) {
		this.id = UUID.randomUUID().toString();
		this.message = message;
		this.emailDest = emailDest;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getEmailDest() {
		return emailDest;
	}

	public void setEmailDest(String emailDest) {
		this.emailDest = emailDest;
	}
}
