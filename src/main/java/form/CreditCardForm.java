package form;

import model.CreditCard;

public class CreditCardForm {
	
	private String nomeTitular;
	private String numCartao;
	private String dataValidade;
	private String cvv;
	
	public CreditCardForm(String nomeTitular, String numCartao, String dataValidade, String cvv){
		this.nomeTitular = nomeTitular;
		this.numCartao = numCartao;
		this.dataValidade = dataValidade;
		this.cvv = cvv;
	}

	public CreditCard deserialize(){
		return new CreditCard(this.nomeTitular, this.numCartao, this.dataValidade, this.cvv);
	}
}
